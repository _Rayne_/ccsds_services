/**
 * @file xemaclite_e.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 27.06.2014
 * Version 1.0.0.0
 */

#ifndef XEMACLITE_E_H_
#define XEMACLITE_E_H_

/***************************** Include Files *********************************/
#include "xemaclite.h"
#include "xstatus.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void EmacLite_PhyDelay(unsigned int Seconds);
void EmacLite_PhyReset(XEmacLite *InstancePtr, u32 PhyAddress);
u32 EmacLite_PhyDetect(XEmacLite *InstancePtr);

#endif /* XEMACLITE_E_H_ */
