/**
 * @file xemaclite_e.c
 *
 * @brief Brief description.
 *
 * Created by Rayne on 27.06.2014
 * Version 1.0.0.0
 */

/***************************** Include Files *********************************/
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>

#include "xemaclite_e.h"

#include "xparameters.h"

/************************** Constant Definitions *****************************/
#define PHY_REG0_OFFSET		0 			// Register 0 of PHY device
#define PHY_REG1_OFFSET 	1 			// Register 1 of PHY device

#define PHY_REG0_RESET_MASK		0x8000 	// Reset Phy device
#define PHY_REG0_LOOPBACK_MASK	0x4000  // Loopback Enable in Phy
#define PHY_REG0_SPD_100_MASK	0x2000  // Speed of 100Mbps for Phy

#define PHY_REG1_DETECT_MASK	0x1808	// Mask to detect PHY device

#define EMACLITE_PHY_DELAY_SEC	4		// Amount of time to delay waiting on PHY to reset

/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
/**
*
* For PPC we use a usleep call, for Microblaze we use an assembly loop that
* is roughly the same regardless of optimization level, although caches and
* memory access time can make the delay vary.  Just keep in mind that after
* resetting or updating the PHY modes, the PHY typically needs time to recover.
*
* @return   None
*
* @note     None
*
******************************************************************************/
void EmacLite_PhyDelay(unsigned int Seconds)
{
#ifdef __MICROBLAZE__
	static int WarningFlag = 0;

	/* If MB caches are disabled or do not exist, this delay loop could
	 * take minutes instead of seconds (e.g., 30x longer).  Print a warning
	 * message for the user (once).  If only MB had a built-in timer!
	 */
	if (((mfmsr() & 0x20) == 0) && (!WarningFlag))
	{
		WarningFlag = 1;
	}

#define ITERS_PER_SEC   (XPAR_CPU_CORE_CLOCK_FREQ_HZ / 6)
    asm volatile ("\n"
                  "1:               \n\t"
                  "addik r7, r0, %0 \n\t"
                  "2:               \n\t"
                  "addik r7, r7, -1 \n\t"
                  "bneid  r7, 2b    \n\t"
                  "or  r0, r0, r0   \n\t"
                  "bneid %1, 1b     \n\t"
                  "addik %1, %1, -1 \n\t"
                  :: "i"(ITERS_PER_SEC), "d" (Seconds));

#else

	usleep(Seconds * 1000000);

#endif
}
/******************************************************************************/
/**
*
* This function set the speed and put the PHY in reset.
*
* @param	InstancePtr is the pointer to the instance of EmacLite driver.
* @param	PhyAddress is the address of the Phy device.
*
* @note		None.
*
******************************************************************************/
void EmacLite_PhyReset(XEmacLite *InstancePtr, u32 PhyAddress)
{
	u16 PhyData = 0;

	PhyData |= PHY_REG0_SPD_100_MASK;
	XEmacLite_PhyWrite(InstancePtr, PhyAddress, PHY_REG0_OFFSET,
	                   PhyData | PHY_REG0_RESET_MASK);

	// Give sufficient delay for Phy Reset.
	EmacLite_PhyDelay(EMACLITE_PHY_DELAY_SEC);
}
/******************************************************************************/
/**
*
* This function detects the PHY address by looking for successful MII status
* register contents (PHY register 1). It looks for a PHY that supports
* auto-negotiation and 10Mbps full-duplex and half-duplex. So, this code
* won't work for PHYs that don't support those features, but it's a bit more
* general purpose than matching a specific PHY manufacturer ID.
*
* Note also that on some (older) Xilinx ML4xx boards, PHY address 0 does not
* properly respond to this query. But, since the default is 0 and assuming
* no other address responds, then it seems to work OK.
*
* @param	InstancePtr is the pointer to the instance of EmacLite driver.
*
* @return	The address of the PHY device detected (returns 0 if not
*		detected).
*
* @note
*		The bit mask (0x1808) of the MII status register
*		(PHY Register 1) used in this function are:
* 		0x1000: 10Mbps full duplex support.
* 		0x0800: 10Mbps half duplex support.
*  		0x0008: Auto-negotiation support.
*
******************************************************************************/
u32 EmacLite_PhyDetect(XEmacLite *InstancePtr)
{
	u16 PhyData;
	int PhyAddr;

	/*
	 * Verify all 32 MDIO ports.
	 */
	for (PhyAddr = 31; PhyAddr >= 0; PhyAddr--) {
		XEmacLite_PhyRead(InstancePtr, PhyAddr, PHY_REG1_OFFSET,
				 &PhyData);

		if (PhyData != 0xFFFF) {
			if ((PhyData & PHY_REG1_DETECT_MASK) ==
			PHY_REG1_DETECT_MASK) {
				return PhyAddr;	/* Found a valid PHY device */
			}
		}
	}
	/*
	 * Unable to detect PHY device returning the default address of 0.
	 */
	return 0;
}

