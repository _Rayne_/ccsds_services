/**
 * @file helloworld.h
 *
 * @brief Brief description.
 *
 * Created by Rayne on 23.06.2014
 * Version 1.0.0.0
 */

#ifndef HELLOWORLD_H_
#define HELLOWORLD_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
//typedef struct hello_world_state {
// char inputbuffer[10];
// char name[40];
//} uip_tcp_appstate_t;
//
//typedef struct hello_world_udp_state {
//	 char inputbuffer[10];
//	 char name[40];
//}uip_udp_appstate_t;

void hello_world_appcall(void);
#ifndef UIP_APPCALL
#define UIP_APPCALL() hello_world_appcall()
#endif /* UIP_APPCALL */

void hello_world_init(void);

/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* HELLOWORLD_H_ */
