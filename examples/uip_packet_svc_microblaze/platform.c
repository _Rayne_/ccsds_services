/******************************************************************************
*
* (c) Copyright 2010-2012 Xilinx, Inc. All rights reserved.
*
* This file contains confidential and proprietary information of Xilinx, Inc.
* and is protected under U.S. and international copyright and other
* intellectual property laws.
*
* DISCLAIMER
* This disclaimer is not a license and does not grant any rights to the
* materials distributed herewith. Except as otherwise provided in a valid
* license issued to you by Xilinx, and to the maximum extent permitted by
* applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL
* FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS,
* IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE;
* and (2) Xilinx shall not be liable (whether in contract or tort, including
* negligence, or under any other theory of liability) for any loss or damage
* of any kind or nature related to, arising under or in connection with these
* materials, including for any direct, or any indirect, special, incidental,
* or consequential loss or damage (including loss of data, profits, goodwill,
* or any type of loss or damage suffered as a result of any action brought by
* a third party) even if such damage or loss was reasonably foreseeable or
* Xilinx had been advised of the possibility of the same.
*
* CRITICAL APPLICATIONS
* Xilinx products are not designed or intended to be fail-safe, or for use in
* any application requiring fail-safe performance, such as life-support or
* safety devices or systems, Class III medical devices, nuclear facilities,
* applications related to the deployment of airbags, or any other applications
* that could lead to death, personal injury, or severe property or
* environmental damage (individually and collectively, "Critical
* Applications"). Customer assumes the sole risk and liability of any use of
* Xilinx products in Critical Applications, subject only to applicable laws
* and regulations governing limitations on product liability.
*
* THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE
* AT ALL TIMES.
*
******************************************************************************/

#include "xparameters.h"
#include "xil_cache.h"
#include "xtmrctr.h"
#include "xintc.h"
#include "xil_exception.h"

#include "platform_config.h"

/*
 * Uncomment the following line if ps7 init source files are added in the
 * source directory for compiling example outside of SDK.
 */
/*#include "ps7_init.h"*/

#ifdef STDOUT_IS_16550
 #include "xuartns550_l.h"

 #define UART_BAUD 9600
#endif

XIntc interruptCtrl;
XTmrCtr tmrCtr;
XEmacLite _XEmacLite;

int platform_time_sec = 0;

void
enable_caches()
{
#ifdef __PPC__
    Xil_ICacheEnableRegion(CACHEABLE_REGION_MASK);
    Xil_DCacheEnableRegion(CACHEABLE_REGION_MASK);
#elif __MICROBLAZE__
#ifdef XPAR_MICROBLAZE_USE_ICACHE
    Xil_ICacheEnable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
    Xil_DCacheEnable();
#endif
#endif
}

void
disable_caches()
{
    Xil_DCacheDisable();
    Xil_ICacheDisable();
}

void
init_uart()
{
#ifdef STDOUT_IS_16550
    XUartNs550_SetBaud(STDOUT_BASEADDR, XPAR_XUARTNS550_CLOCK_HZ, UART_BAUD);
    XUartNs550_SetLineControlReg(STDOUT_BASEADDR, XUN_LCR_8_DATA_BITS);
#endif
#ifdef STDOUT_IS_PS7_UART
    /* Bootrom/BSP configures PS7 UART to 115200 bps */
#endif
}

void platform_timer_handler(void *CallBackRef, u8 TmrCtrNumber)
{
	/* 0 is XTMR_CTR_1_HZ_TIMER_N 1 second interval */
	if(TmrCtrNumber == 0)
		platform_time_sec++;
}

void platform_setup_timer()
{
    /* init timer */
	XTmrCtr_Initialize(&tmrCtr, XPAR_TMRCTR_0_DEVICE_ID);

	XTmrCtr_SetOptions(&tmrCtr, XTMR_CTR_1_HZ_TIMER_N, XTC_INT_MODE_OPTION | XTC_DOWN_COUNT_OPTION | XTC_AUTO_RELOAD_OPTION);

	XTmrCtr_SetResetValue(&tmrCtr, XTMR_CTR_1_HZ_TIMER_N, XPAR_TMRCTR_0_CLOCK_FREQ_HZ);

	XTmrCtr_SetHandler(&tmrCtr, platform_timer_handler, NULL);

	XTmrCtr_Start(&tmrCtr, XTMR_CTR_1_HZ_TIMER_N);
}

void platform_setup_interrupts()
{
	XIntc_Initialize(&interruptCtrl, XPAR_INTC_0_DEVICE_ID);

	XIntc_Enable(&interruptCtrl, XPAR_MICROBLAZE_0_INTC_ETHERNET_LITE_IP2INTC_IRPT_INTR);

	XIntc_Connect(&interruptCtrl, XPAR_INTC_0_TMRCTR_0_VEC_ID, XTmrCtr_InterruptHandler, &tmrCtr);

	XIntc_Enable(&interruptCtrl, XPAR_MICROBLAZE_0_INTC_AXI_TIMER_0_INTERRUPT_INTR);

	XIntc_Start(&interruptCtrl, XIN_REAL_MODE);

// Don't know what fore this line is.
//	XIntc_MasterEnable(XPAR_INTC_0_BASEADDR);

	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(XExceptionHandler)XIntc_InterruptHandler,
			(void*)&interruptCtrl);
}

void platform_enable_interrupts()
{
	Xil_ExceptionEnable();
}

int platform_get_time()
{
	return platform_time_sec;
}

int platform_setup_emac()
{
	int Status;
	XEmacLite_Config *ConfigPtr;
	unsigned int PhyAddress = 0;
	/*
	 * Initialize the EmacLite device.
	 */
	ConfigPtr = XEmacLite_LookupConfig(0);
	if (ConfigPtr == NULL) {
		return XST_FAILURE;
	}
	Status = XEmacLite_CfgInitialize(&_XEmacLite, ConfigPtr, ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the MAC address.
	 */
	unsigned char mac_ethernet_address[6] = XEMACLITE_MAC_INITIALIZER;
	XEmacLite_SetMacAddress(&_XEmacLite, mac_ethernet_address);

	/*
	 * Empty any existing receive frames.
	 */
	XEmacLite_FlushReceive(&_XEmacLite);

	/*
	 * Check if there is a TX buffer available, if there isn't it is an
	 * error.
	 */
	if (XEmacLite_TxBufferAvailable(&_XEmacLite) != TRUE) {
		return XST_FAILURE;
	}

	/*
	 * If the MDIO is configured in the device.
	 */
	if (XEmacLite_IsMdioConfigured(&_XEmacLite)) {
		/*
		 * Detect the PHY device and enable the MAC Loop back
		 * in the PHY.
		 */
		PhyAddress = EmacLite_PhyDetect(&_XEmacLite);
		EmacLite_PhyReset(&_XEmacLite, PhyAddress);
	}

	//XEmacLite_Send(EmacLiteInstPtr, uip_buf, 20);

	return XST_SUCCESS;
}

int platform_emac_recv(u8* buff)
{
	return XEmacLite_Recv(&_XEmacLite, buff);
}

void platform_emac_send(u8* buff, unsigned int len)
{
	if (!XEmacLite_TxBufferAvailable(&_XEmacLite)){
		//xil_printf("Send: blocked \r\n");
		 return;
	}

	XEmacLite_Send(&_XEmacLite, buff, len);
}

void init_platform()
{
    /*
     * If you want to run this example outside of SDK,
     * uncomment the following line and also #include "ps7_init.h" at the top.
     * Make sure that the ps7_init.c and ps7_init.h files are included
     * along with this example source files for compilation.
     */
    /* ps7_init();*/
    enable_caches();
    init_uart();
	platform_setup_timer();
	platform_setup_interrupts();
	platform_enable_interrupts();
//	platform_setup_emac();
}

void
cleanup_platform()
{
    disable_caches();
}


