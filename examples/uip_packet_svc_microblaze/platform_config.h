#ifndef __PLATFORM_CONFIG_H_
#define __PLATFORM_CONFIG_H_

#ifdef __PPC__
#define CACHEABLE_REGION_MASK 0x80800400
#endif

#include "../drivers/emaclite_v3_04_a/src/xemaclite_e.h"

#define XTMR_CTR_1_HZ_TIMER_N			0

#define XEMACLITE_MAC_INITIALIZER		{ 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 }


#endif
