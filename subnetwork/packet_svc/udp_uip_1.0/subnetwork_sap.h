/*
 * subnetwork_sap.h
 *
 *  Created on: 24.03.2014
 *      Author: Rayne
 */

#ifndef SUBNETWORK_SAP_H_
#define SUBNETWORK_SAP_H_

/***************************** Include Files *********************************/
/************************** Constant Definitions *****************************/
#define SUBNETWORK_ANY_SAP				0
#define SUBNETWORK_MY_PC_SAP			1

/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/


#endif /* SUBNETWORK_SAP_H_ */
