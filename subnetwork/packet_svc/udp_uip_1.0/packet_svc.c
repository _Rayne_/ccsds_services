/*
 * packet_svc.c
 *
 * @brief Implementation of packet svc for uIP.
 *
 *  Created on: 11.12.2013
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "packet_svc.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "uip.h"
#include "uip_arp.h"
#include "tapdev.h"

/************************** Constant Definitions *****************************/
#define PACKET_SVC_LOCAL_PORT_START			501

#define PACKET_SVC_SAP_ID_TO_PORT(id)		((id) + PACKET_SVC_LOCAL_PORT_START)
#define PACKET_SVC_PORT_TO_SAP_ID(port)		((port) - PACKET_SVC_LOCAL_PORT_START)

/**************************** Type Definitions *******************************/

/***************** Macros (Inline Functions) Definitions *********************/
#define u32ip_to_uip_copy(dest, src)			memcpy(dest, src, sizeof(uint32_t))
#define u32ip_to_uip_cmp(addr1, addr2) 			memcmp(addr1, addr2, sizeof(uint32_t))

#define uip_conn_accept_multiple_ip(conn)		(((conn)->ripaddr[0] == 0) && ((conn)->ripaddr[1] == 0))
#define uip_conn_accept_multiple_port(conn)		((conn)->rport == PACKET_SVC_PORT_ANY)

#define conn_by_sap_id(sap_id)					((struct uip_udp_conn *)EthernetSaps[(sap_id)].Pcb)

//inline void PacketSVC_ReadEthSapAddrFromCurUdpHdr(PacketSvcEthSap* sapPtr)
//{
//	static struct uip_udpip_hdr *hdr;
//	hdr = (struct uip_udpip_hdr *)&uip_buf[UIP_LLH_LEN];
//
//	//copy remote IP and Port
//	u32ip_to_uip_copy(&(sapPtr->Addr), hdr->srcipaddr);
//	//copy Host Port
//	sapPtr->Port = HTONS(hdr->srcport);
//}

//inline void PacketSVC_RewriteEthSapConnection(PacketSvcEthSap* sapPtr)
//{
//	struct uip_udp_conn * conn = sapPtr->Pcb;
//	u32ip_to_uip_copy(conn->ripaddr, &(sapPtr->Addr));
//	conn->rport = HTONS(sapPtr->Port);
//}


/************************** Variable Definitions *****************************/
extern PacketSvcEthSap EthernetSaps[PSVC_ETH_SAPS_MAX];

extern uint16_t uip_slen;

/************************** Function Prototypes ******************************/
void PacketSVC_Failure();

/************************ Function Implementations ***************************/

 void uip_udp_packet_send(struct uip_udp_conn *c, const void *data, int len)
 {
 #if UIP_UDP
   if(data != NULL) {
     uip_udp_conn = c;
     uip_slen = len;
     memcpy(&uip_buf[UIP_LLH_LEN + UIP_IPUDPH_LEN], data,
            len > UIP_BUFSIZE? UIP_BUFSIZE: len);
     uip_process(UIP_UDP_SEND_CONN);
 #if UIP_CONF_IPV6
     tcpip_ipv6_output();
 #else
     if(uip_len > 0) {
		  uip_arp_out();
		  tapdev_send();
     }
 #endif
   }
   uip_slen = 0;
 #endif /* UIP_UDP */
 }

 void uip_udp_packet_sendto(struct uip_udp_conn *c, const void *data, int len,
                       const uip_ipaddr_t *toaddr, uint16_t toport)
 {
   uip_ipaddr_t curaddr;
   uint16_t curport;

   if(toaddr != NULL) {
     /* Save current IP addr/port. */
     uip_ipaddr_copy(&curaddr, &c->ripaddr);
     curport = c->rport;

     /* Load new IP addr/port */
     uip_ipaddr_copy(&c->ripaddr, toaddr);
     c->rport = toport;

     uip_udp_packet_send(c, data, len);

     /* Restore old IP addr/port */
     uip_ipaddr_copy(&c->ripaddr, &curaddr);
     c->rport = curport;
   }
 }


/**
 * @brief Initialize packet svc.
 *
 * Creates appropriate connections or pcbs for declared saps and bind them together.
 */
void PacketSVC_Init()
{
	struct uip_udp_conn *conn;

	/* bind ports for those saps that have callback */
	int i = 0;
	for (i = 0; i < PSVC_ETH_SAPS_MAX; ++i) {
		if(EthernetSaps[i].Callback == NULL)
			continue;

		uip_ipaddr_t addr;
		u32ip_to_uip_copy(addr, &(EthernetSaps[i].Addr));
		conn = uip_udp_new(&addr, HTONS(EthernetSaps[i].Port));
		if(conn!= NULL) {
			uip_udp_bind(conn, HTONS(PACKET_SVC_SAP_ID_TO_PORT(i)));
			EthernetSaps[i].Pcb = conn;
		}
	}
}

void PacketSVC_Entry()
{
	if (uip_newdata())
	{
		int sapId = PACKET_SVC_PORT_TO_SAP_ID(HTONS(uip_udp_conn->lport));
		if(sapId >= PSVC_ETH_SAPS_MAX)
			return;

		/* no callback */
		if(EthernetSaps[sapId].Callback == NULL)
			return;

		static struct uip_udpip_hdr *hdr;
		hdr = (struct uip_udpip_hdr *)&uip_buf[UIP_LLH_LEN];

		if(uip_conn_accept_multiple_ip(uip_udp_conn))
			u32ip_to_uip_copy(&(EthernetSaps[sapId].Addr), hdr->srcipaddr);

		if(uip_conn_accept_multiple_port(uip_udp_conn))
			EthernetSaps[sapId].Port = HTONS(hdr->srcport);

		EthernetSaps[sapId].Callback(
				sapId,
				0,
				PACKET_CAHNNEL_ETHERNET, uip_appdata, uip_datalen());
	}
}

//void PacketSVC_Receive(PacketSVC_RecvCallback callback)
//{
//	RecvCallback = callback;
//
//}

/**
 * @brief Sends specified amount of data using specified channel.
 *
 * @note If access point supports multiple connections from any address or any port,
 * sending will be done to last received packet source for that point.
 *
 * @param psSapId
 * @param pdSapId
 * @param channel Data channel.
 * @param data Data buffer pointer.
 * @param len Amount of bytes to send.
 * @return Send status.
 * 	@arg #PACKET_SVC_OK - success send.
 * 	@arg #PACKET_SVC_INVALID_CHANNEL - invalid channel.
 * 	@arg #PACKET_SVC_INVALID_SAP - invalid pdsap.
 * 	@arg #PACKET_SVC_ERROR - general error.
 */
int PacketSVC_Send(u16 psSapId, u16 pdSapId, u8 channel, u8* data, int len)
{
	if(pdSapId >= PSVC_ETH_SAPS_MAX)
		return PACKET_SVC_INVALID_SAP;

	if(EthernetSaps[pdSapId].Pcb == NULL)
	{
		uip_ipaddr_t addr;
		u32ip_to_uip_copy(addr, &(EthernetSaps[pdSapId].Addr));
		struct uip_udp_conn * conn = uip_udp_new(&addr, HTONS(EthernetSaps[pdSapId].Port));
		if(conn!= NULL) {
			uip_udp_bind(conn, HTONS(PACKET_SVC_SAP_ID_TO_PORT(pdSapId)));
			EthernetSaps[pdSapId].Pcb = conn;
		}else
			return PACKET_SVC_ERROR;
	}

	switch (channel) {
		case PACKET_CAHNNEL_ETHERNET:

			xil_printf("%d %d %d %d\r\n",
					((uip_udp_conn->ripaddr[0] & 0xFF00) >> 8),
					(uip_udp_conn->ripaddr[0] & 0xFF),
					((uip_udp_conn->ripaddr[1] & 0xFF00) >> 8),
					(uip_udp_conn->ripaddr[1] & 0xFF));

			xil_printf("%d -> %d\r\n",HTONS(uip_udp_conn->lport), HTONS(uip_udp_conn->rport));

			struct uip_udp_conn * conn = EthernetSaps[pdSapId].Pcb;

			if(uip_conn_accept_multiple_ip(conn) || uip_conn_accept_multiple_port(conn))
			{
				uip_ipaddr_t toaddr;
				u32ip_to_uip_copy(toaddr, &(EthernetSaps[pdSapId].Addr));

				uip_udp_packet_sendto(conn, data, len,
						(const uip_ipaddr_t*)&toaddr,
						HTONS(EthernetSaps[pdSapId].Port));
			}else
			{
				uip_udp_packet_send(conn, data, len);
			}

			return PACKET_SVC_OK;
		default:
			return PACKET_SVC_INVALID_CHANNEL;
	}
	return PACKET_SVC_ERROR;
}
