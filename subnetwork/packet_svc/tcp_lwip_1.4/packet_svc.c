/*
 * packet_svc.c
 *
 *  Created on: 11.12.2013
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "packet_svc.h"

#include "lwip/err.h"
#include "lwip/tcp.h"

/************************** Constant Definitions *****************************/
//TODO: Redefine port number, to connect first sap.
#define PACKET_SVC_LOCAL_PORT_START			7

/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
#define PACKET_SVC_SAP_ID_TO_PORT(id)		((id) + PACKET_SVC_LOCAL_PORT_START)
#define PACKET_SVC_PORT_TO_SAP_ID(port)		((port) - PACKET_SVC_LOCAL_PORT_START)

/************************** Variable Definitions *****************************/
//static struct udp_pcb * pcb = NULL;
//static struct ip_addr addr_last;
//static u16_t port_last = 0;

extern PacketSvcEthSap EthernetSaps[PSVC_ETH_SAPS_MAX];

/************************** Function Prototypes ******************************/
void PacketSVC_Failure();

err_t tcp_recv_handler(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err){
	xil_printf("received at %d, echoing to the same port\r\n",pcb->local_port);

	xil_printf("arg = %d\r\n", arg);

	/* do not read the packet if we are not in ESTABLISHED state */
	if (p != NULL) {

		/* indicate that the packet has been received */
		tcp_recved(pcb, p->len);

		int sapId = PACKET_SVC_PORT_TO_SAP_ID(pcb->local_port);
		if(sapId >= PSVC_ETH_SAPS_MAX)
			return ERR_OK;

		//Check is multiconnection and save addr if so.
		if(ip_addr_isany(&(pcb->local_ip)))
		{
			EthernetSaps[sapId].Addr = pcb->remote_ip.addr;
			/* do not save port because lwip does not support any port receive */
//			EthernetSaps[sapId].Port = port;
		}

		xil_printf("TCP rcv %d bytes\r\n", (*p).len);

		/* no callback */
		if(EthernetSaps[sapId].Callback == NULL)
			return ERR_OK;

		int err = EthernetSaps[sapId].Callback(sapId, 0, PACKET_CAHNNEL_ETHERNET, (u8*)(p->payload), p->len);

		if (err)
		  xil_printf("Protocol error : %d\r\n", err);

		pbuf_free(p);
	}
	else
	{
		tcp_close(pcb);
		tcp_recv(pcb, NULL);
		xil_printf("... closed.\r\n");
	}

	return ERR_OK;
}

err_t tcp_accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{
	int sapId = PACKET_SVC_PORT_TO_SAP_ID(newpcb->local_port);

	xil_printf("\r\nSAP (%d) Accepted\n\r", sapId);

	EthernetSaps[sapId].Pcb = newpcb;

	/* set the receive callback for this connection */
	tcp_recv(newpcb, tcp_recv_handler);

	/* just use an integer number indicating the connection id as the
	   callback argument */
	tcp_arg(newpcb, (void*)sapId);

	return ERR_OK;
}

/************************ Function Implementations ***************************/
void PacketSVC_Init()
{
	struct tcp_pcb * pcb;

	/* bind ports for those saps that have callback */
	int i = 0;
	for (i = 0; i < PSVC_ETH_SAPS_MAX; ++i) {
		if(EthernetSaps[i].Callback == NULL)
			continue;

		pcb = tcp_new();

		if(pcb!= NULL) {
			tcp_bind(pcb, IP_ADDR_ANY, PACKET_SVC_SAP_ID_TO_PORT(i));
			tcp_arg(pcb, NULL);
			pcb = tcp_listen(pcb);
			tcp_accept(pcb, tcp_accept_callback);
			EthernetSaps[i].Pcb = pcb;
		}
	}
}

//void PacketSVC_Receive(PacketSVC_RecvCallback callback)
//{
//	RecvCallback = callback;
//
//}

void PacketSVC_Failure()
{

}

/**
 * @brief Sends specified amount of data using specified channel.
 *
 * @param psasp
 * @param pdsap
 * @param channel Data channel.
 * @param data Data buffer pointer.
 * @param len Amount of bytes to send.
 * @return Send status.
 * 	@arg #PACKET_SVC_OK - success send.
 * 	@arg #PACKET_SVC_INVALID_CHANNEL - invalid channel.
 * 	@arg #PACKET_SVC_INVALID_SAP - invalid pdsap.
 * 	@arg #PACKET_SVC_ERROR - general error.
 */
int PacketSVC_Send(u16 psSapId, u16 pdSapId, u8 channel, u8* data, int len)
{
	if(pdSapId >= PSVC_ETH_SAPS_MAX)
		return PACKET_SVC_INVALID_SAP;

	struct tcp_pcb * pcb = EthernetSaps[pdSapId].Pcb;

	if(pcb == NULL)
	{
		/* service could not establish connection by itself */
		return PACKET_SVC_INVALID_SAP;
	}


	err_t err;

	switch (channel) {
		case PACKET_CAHNNEL_ETHERNET:
			err = tcp_write(pcb, data, len, TCP_WRITE_FLAG_COPY);

			xil_printf("TCP sent %d bytes\r\n", len);
			if(err != ERR_OK)
				xil_printf("TCP send error %d\r\n", err);

			return PACKET_SVC_OK;
		default:
			return PACKET_SVC_INVALID_CHANNEL;
	}
	return PACKET_SVC_ERROR;
}
