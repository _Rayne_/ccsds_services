/*
 * packet_svc.h
 *
 *  Created on: 11.12.2013
 *      Author: Rayne
 */

#ifndef PACKET_SVC_H_
#define PACKET_SVC_H_

/***************************** Include Files *********************************/
#include <xil_types.h>
#include <stdint.h>
#include "subnetwork_sap.h"

/************************** Constant Definitions *****************************/
#define PACKET_CAHNNEL_ETHERNET			0
#define PACKET_CAHNNEL_1553B			1

#define PSVC_ETH_SAPS_MAX				10

#define PACKET_SVC_ADDR_ANY				0
#define PACKET_SVC_PORT_ANY				0

#define PSVC_ETH_UIP

/**************************** Type Definitions *******************************/
#if defined(PSVC_ETH_UIP)
typedef struct hello_world_state {
 char inputbuffer[10];
} uip_tcp_appstate_t;

typedef struct hello_world_udp_state {
	 char inputbuffer[10];
}uip_udp_appstate_t;
#endif



typedef int (*PacketSVC_RecvCallback)(u16 psSapId, u16 pdSapId, u8 channel, u8* data, int len);

typedef struct {
	uint32_t Addr;
	uint16_t Port;
	PacketSVC_RecvCallback Callback;

	void * Pcb;
}PacketSvcEthSap;

/***************** Macros (Inline Functions) Definitions *********************/
#define PACKET_SVC_OK							0
#define PACKET_SVC_ERROR						1
#define PACKET_SVC_INVALID_CHANNEL				2
#define PACKET_SVC_INVALID_SAP					3

/************************** Variable Definitions *****************************/
/************************** Function Prototypes ******************************/
void PacketSVC_Init();

void PacketSVC_Entry();

//void PacketSVC_Receive(PacketSVC_RecvCallback callback);

int PacketSVC_Send(u16 psSapId, u16 pdSapId, u8 channel, u8* data, int len);

#if defined(PSVC_ETH_UIP)
#define UIP_UDP_APPCALL()		PacketSVC_Entry()
#define UIP_APPCALL()
#endif

#endif /* PACKET_SVC_H_ */
