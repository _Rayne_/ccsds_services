/*
 * packet_svc.c
 *
 *  Created on: 11.12.2013
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "packet_svc.h"

#include "lwip/err.h"
#include "lwip/udp.h"

/************************** Constant Definitions *****************************/
#define PACKET_SVC_LOCAL_PORT_START			501

/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
#define PACKET_SVC_SAP_ID_TO_PORT(id)		((id) + PACKET_SVC_LOCAL_PORT_START)
#define PACKET_SVC_PORT_TO_SAP_ID(port)		((port) - PACKET_SVC_LOCAL_PORT_START)

/************************** Variable Definitions *****************************/
//static struct udp_pcb * pcb = NULL;
//static struct ip_addr addr_last;
//static u16_t port_last = 0;

extern PacketSvcEthSap EthernetSaps[PSVC_ETH_SAPS_MAX];

/************************** Function Prototypes ******************************/
void PacketSVC_Failure();

void udp_recv_handler(void *arg, struct udp_pcb *pcb, struct pbuf *p, struct ip_addr *addr, u16_t port){
	xil_printf("received at %d, echoing to the same port\r\n",pcb->local_port);
	//dst_ip = &(pcb->remote_ip); // this is zero always
	if (p != NULL) {

		int sapId = PACKET_SVC_PORT_TO_SAP_ID(port);
		if(sapId >= PSVC_ETH_SAPS_MAX)
			return;

		//Check is multiconnection and save addr if so.
		if(ip_addr_isany(&(pcb->local_ip)))
		{
			EthernetSaps[sapId].Addr = addr->addr;
			/* do not save port because lwip does not support any port receive */
//			EthernetSaps[sapId].Port = port;
		}

		xil_printf("UDP rcv %d bytes\r\n", (*p).len);

		/* no callback */
		if(EthernetSaps[sapId].Callback == NULL)
			return;

		int err = EthernetSaps[sapId].Callback(sapId, 0, PACKET_CAHNNEL_ETHERNET, (u8*)(p->payload), p->len);

		if (err)
		  xil_printf("Protocol error : %d\r\n", err);

		pbuf_free(p);
	}
}

/************************ Function Implementations ***************************/
void PacketSVC_Init()
{
	struct udp_pcb * pcb;

	/* bind ports for those saps that have callback */
	int i = 0;
	for (i = 0; i < PSVC_ETH_SAPS_MAX; ++i) {
		if(EthernetSaps[i].Callback == NULL)
			continue;

		pcb = udp_new();

		if(pcb!= NULL) {
			udp_bind(pcb, IP_ADDR_ANY, PACKET_SVC_SAP_ID_TO_PORT(i));
			udp_recv(pcb, udp_recv_handler, NULL);
			EthernetSaps[i].Pcb = pcb;
		}
	}
}

//void PacketSVC_Receive(PacketSVC_RecvCallback callback)
//{
//	RecvCallback = callback;
//
//}

void PacketSVC_Failure()
{

}

/**
 * @brief Sends specified amount of data using specified channel.
 *
 * @param psasp
 * @param pdsap
 * @param channel Data channel.
 * @param data Data buffer pointer.
 * @param len Amount of bytes to send.
 * @return Send status.
 * 	@arg #PACKET_SVC_OK - success send.
 * 	@arg #PACKET_SVC_INVALID_CHANNEL - invalid channel.
 * 	@arg #PACKET_SVC_INVALID_SAP - invalid pdsap.
 * 	@arg #PACKET_SVC_ERROR - general error.
 */
int PacketSVC_Send(u16 psSapId, u16 pdSapId, u8 channel, u8* data, int len)
{
	if(pdSapId >= PSVC_ETH_SAPS_MAX)
		return PACKET_SVC_INVALID_SAP;

	struct udp_pcb * pcb = EthernetSaps[pdSapId].Pcb;

	if(pcb == NULL)
	{
		pcb = udp_new();

		if(pcb!= NULL) {
			udp_bind(pcb, IP_ADDR_ANY, PACKET_SVC_SAP_ID_TO_PORT(pdSapId));
			udp_recv(pcb, udp_recv_handler, NULL);
			EthernetSaps[pdSapId].Pcb = pcb;
		}else
			return PACKET_SVC_ERROR;
	}

	static struct ip_addr addr;

	struct pbuf * pb;
	switch (channel) {
		case PACKET_CAHNNEL_ETHERNET:

			pb = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_REF);
			pb->payload = data;
			pb->len = pb->tot_len = len;

			/* always send to saved address and port */
			addr.addr = EthernetSaps[pdSapId].Addr;
			err_t err = udp_sendto(pcb, pb, &addr, EthernetSaps[pdSapId].Port);

			xil_printf("UDP sent %d bytes\r\n", len);
			if(err != ERR_OK)
				xil_printf("UDP send error %d\r\n", err);

			pbuf_free(pb);

			return PACKET_SVC_OK;
		default:
			return PACKET_SVC_INVALID_CHANNEL;
	}
	return PACKET_SVC_ERROR;
}
