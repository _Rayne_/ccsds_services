/*
 * packet_svc_eth_saps.c
 *
 *  Created on: 21.03.2014
 *      Author: Rayne
 */

/***************************** Include Files *********************************/
#include "packet_svc.h"
#include "subnetwork_sap.h"
#include "../pus/pus_application.h"

/************************** Constant Definitions *****************************/
#define PSCV_IP_ANY_UNKNOWN				0

/**************************** Type Definitions *******************************/


/***************** Macros (Inline Functions) Definitions *********************/
//#define ETHERNET_ROUTES_END 		{.SAP = 0, .Addr = {PSCV_IP_ANY_UNKNOWN}, .Port = UDP_RX_PORT, .Callback = PUSAPP_TCReceive},
#define PSCV_IP4_ADDR(a,b,c,d) \
        ((u32)((d) & 0xff) << 24) | \
        ((u32)((c) & 0xff) << 16) | \
        ((u32)((b) & 0xff) << 8)  | \
        (u32)((a) & 0xff)

#define PSCV_ETH_ROUTES_BEGIN(name)								PacketSvcEthSap name[PSVC_ETH_SAPS_MAX]={
#define PSCV_ETH_ROUTE(id, addr, port, recv)					[(id)] = {.Addr = (addr), .Port = (port), .Callback = (recv), .Pcb = NULL}
#define PSCV_ETH_ROUTES_END()									}

/************************** Variable Definitions *****************************/
/* SVC SAP should start from 1 */
PSCV_ETH_ROUTES_BEGIN(EthernetSaps)
PSCV_ETH_ROUTE(SUBNETWORK_MY_PC_SAP, PSCV_IP_ANY_UNKNOWN, 501, PUSAPP_TCReceive),
PSCV_ETH_ROUTE(2, PSCV_IP4_ADDR(192,168,231,238), 600, PUSAPP_TCReceive)
PSCV_ETH_ROUTES_END();

/************************** Function Prototypes ******************************/
/************************ Function Implementations ***************************/
//PacketSvcEthSap* PacketSVC_GetEthernetRouteBySAP(u16 sap)
//{
//	int i = 0;
//	for (i = 0; i < PSVC_ETHERNET_ROUTES_MAX; ++i) {
//		if(EthernetRoutes[i].SAP == sap)
//			return EthernetRoutes + i;
//	}
//	return NULL;
//}
//
//PacketSvcEthSap* PacketSVC_GetEthernetRouteByAddr(struct ip_addr ip, u16 port)
//{
//	int i = 0;
//	for (i = 0; i < PSVC_ETHERNET_ROUTES_MAX; ++i) {
//		if((memcmp(&(EthernetRoutes[i].Addr), &ip, sizeof(ip)) == 0) && (port == EthernetRoutes[i].Port))
//			return EthernetRoutes + i;
//	}
//
//	for (i = 0; i < PSVC_ETHERNET_ROUTES_MAX; ++i) {
//		if(EthernetRoutes[i].Addr.addr == PSCV_IP_ANY_UNKNOWN)
//			return EthernetRoutes + i;
//	}
//
//	return NULL;
//}
